import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { logOutService } from '../logOut.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  online = true;
  constructor(private logOutService: logOutService) { }

  ngOnInit(): void {
  }
  toggle(event){
    this.online = event.checked;
  }
  logOut(){
    this.logOutService.logOut();
  }
}

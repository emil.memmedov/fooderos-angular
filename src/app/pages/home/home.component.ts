import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { logOut } from './homeGuard.service';
import { logOutService } from './logOut.service';
import { createGuardPageInt } from './main/createPageGuard.service';
import { MainService } from './main/main.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit, createGuardPageInt {

  constructor(private router: Router, private mainService: MainService, private logOutService: logOutService) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.logOutService.logOut();
    }, JSON.parse(localStorage.getItem('fooderos')).expires_in);
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean{
    if(this.mainService.updated){
      return true;
    }
    else{
      return confirm("Do you want to discard changes?");
    }
  }
}

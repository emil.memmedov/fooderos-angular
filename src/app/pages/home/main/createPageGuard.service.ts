import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

export interface createGuardPageInt{
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({providedIn: 'root'})
export class createGuardPage implements CanDeactivate<createGuardPageInt>{
  canDeactivate(component: createGuardPageInt, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean{
    return component.canDeactivate();
  }
}

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ApiService } from '../../ApiService.service';
import { errorService } from '../../popup/error.service';

@Injectable({providedIn: 'root'})
export class MainService{
  menu = 2;
  menuEmit = new Subject<number>();
  public tagTypes;
  updated = true;
  editingTag;
  editMode = false;
  editEmit = new Subject();
  constructor(private http: HttpClient, private apiService: ApiService, private error: errorService, private router: Router){
    this.http.get(this.apiService.rest.url + 'v1/admin/tag/types', {
      headers: new HttpHeaders({
        //@ts-ignore
        'Authorization': 'Bearer' + JSON.parse(localStorage.getItem('fooderos')).token.toString(),
        'ApplicationKey': this.apiService.rest.admin_key,
        'Content-Type': 'application/json'
      })
    }).subscribe(data => {
      //@ts-ignore
      this.tagTypes = data.content;
    }, error => {
      console.log('Error', error);
    });
  }
  changeMenu(newMenu){
    this.menu = newMenu;
    this.menuEmit.next(this.menu);
  }
  changeUpdated(status){
    this.updated = status;
  }
  getTags(){
    let params = new HttpParams();
    params = params.append('tag_type', '1');
    params = params.append('per_page', '10');
    params = params.append('store_type', '1');
    return this.http.get(this.apiService.rest.url + 'v1/admin/tag', {
      headers: new HttpHeaders({
        //@ts-ignore
        'Authorization': 'Bearer' + JSON.parse(localStorage.getItem('fooderos')).token.toString(),
        'ApplicationKey': this.apiService.rest.admin_key,
        'Content-Type': 'application/json'
      }),
      params : params
    });
  }
  loadMore(tagsNumber){
    let params = new HttpParams();
    params = params.append('tag_type', '1');
    params = params.append('per_page', tagsNumber.toString());
    params = params.append('store_type', '1');
    return this.http.get(this.apiService.rest.url + 'v1/admin/tag', {
      headers: new HttpHeaders({
        //@ts-ignore
        'Authorization': 'Bearer' + JSON.parse(localStorage.getItem('fooderos')).token.toString(),
        'ApplicationKey': this.apiService.rest.admin_key,
        'Content-Type': 'application/json'
      }),
      params : params
    });
  }
  createTag(locales, tagType, media){
    this.http.post(this.apiService.rest.url + 'v1/admin/tag',
    {tag_type_id: +tagType, store_type_id: 1, locales: locales, media: media},
    {
      headers: new HttpHeaders({
        //@ts-ignore
        'Authorization': 'Bearer' + JSON.parse(localStorage.getItem('fooderos')).token.toString(),
        'ApplicationKey': this.apiService.rest.admin_key,
        'Content-Type': 'application/json'
      })
    }).subscribe(data => {
      console.log(data);
    });
  }
  changeTagStatus(status, id){
    this.http.put(this.apiService.rest.url + 'v1/admin/tag/status/' + id, {status: status},{
      headers: new HttpHeaders({
        //@ts-ignore
        'Authorization': 'Bearer' + JSON.parse(localStorage.getItem('fooderos')).token.toString(),
        'ApplicationKey': this.apiService.rest.admin_key,
        'Content-Type': 'application/json'
      })
    }).subscribe(data => {
      console.log(data);
    });
  }
  deleteTag(id){
    this.http.delete(this.apiService.rest.url + 'v1/admin/tag/' + id,{
      headers: new HttpHeaders({
        //@ts-ignore
        'Authorization': 'Bearer' + JSON.parse(localStorage.getItem('fooderos')).token.toString(),
        'ApplicationKey': this.apiService.rest.admin_key,
        'Content-Type': 'application/json'
      })
    }).subscribe(data => {
      location.reload();
      console.log(data);
    });
  }
  editTag(id){
    this.http.get(this.apiService.rest.url + 'v1/admin/tag/show/' + id,{
      headers: new HttpHeaders({
        //@ts-ignore
        'Authorization': 'Bearer' + JSON.parse(localStorage.getItem('fooderos')).token.toString(),
        'ApplicationKey': this.apiService.rest.admin_key,
        'Content-Type': 'application/json'
      })
    }).subscribe(data => {
      //@ts-ignore
      this.editEmit.next({mode: true, tag: data.content});
      this.router.navigate(['/home/edit']);
    });
  }
  Edit(tagType, locales){
    // this.http.put(this.apiService.rest.url + 'v1/admin/tag/status/' + id, {tag_type_id: +tagType, store_type_id: 1, locales: locales, media: media},{
    //   headers: new HttpHeaders({
    //     //@ts-ignore
    //     'Authorization': 'Bearer' + JSON.parse(localStorage.getItem('fooderos')).token.toString(),
    //     'ApplicationKey': this.apiService.rest.admin_key,
    //     'Content-Type': 'application/json'
    //   })
    // }).subscribe(data => {
    //   console.log(data);
    // });
  }
}

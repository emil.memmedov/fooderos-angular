import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../ApiService.service';
import { MainService } from './main.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit{
  menu = 2;
  tags;
  totalTags;
  currentTags = 10;
  language = [true, false, false, false];
  activeLanguageIndex = 0;
  url: any = false;
  locales = [];
  tagName = '';
  updated = true;
  editingTag;
  editingId;
  editMode = false;
  @ViewChild('inpTag') inpTag: ElementRef;
  @ViewChild('tagType') tagType: ElementRef;

  constructor(private mainService: MainService, private apiService: ApiService, private router: Router, private route: ActivatedRoute) {
    if(this.route.snapshot.routeConfig.path === 'home/create'){
      this.mainService.changeMenu(20);
    }
    if(this.route.snapshot.routeConfig.path === 'home/edit'){
      this.mainService.changeMenu(20);
    }
  }
  ngOnInit(): void {
    this.menu = this.mainService.menu;
    this.mainService.getTags().subscribe(data => {
      //@ts-ignore
      this.tags = data.content;
      //@ts-ignore
      this.totalTags = data.content.total;
    }, error => {
      console.log('Error', error);
    });
    this.mainService.menuEmit.subscribe(menu => {this.menu = menu });
    this.mainService.editEmit.subscribe(tag => {
      //@ts-ignore
      this.editMode = tag.mode;
      //@ts-ignore
      this.editingTag = tag.tag;
      //@ts-ignore
      this.locales = tag.tag.locales;
    });
  }
  LangClick(i){
    this.language = [false, false, false, false];
    this.language[i] = true;
    this.activeLanguageIndex = i;
    let lang;
    if(this.language[0]){
      lang = 'az';
    }
    if(this.language[1]){
      lang = 'en';
    }
    if(this.language[2]){
      lang = 'ru';
    }
    if(this.language[3]){
      lang = 'tr';
    }
    let tag = this.locales.find(locale => {
      return locale.locale === lang;
    });
    if(tag !== undefined){
      this.inpTag.nativeElement.value = tag.title;
    }
    else{
      this.inpTag.nativeElement.value = '';
    }
  }
  loadMore(){
    if(this.currentTags !== this.totalTags){
      this.currentTags += 10;
      if(this.currentTags > this.totalTags){
        this.currentTags = this.totalTags;
      }
      this.mainService.loadMore(this.currentTags).subscribe(data => {
        //@ts-ignore
        this.tags = data.content;
        //@ts-ignore
        this.totalTags = data.content.total;
      }, error => {
        console.log('Error', error);
      });
    }
  }
  newTag(){
    this.router.navigate(['/home/create']);
    this.mainService.changeMenu(20);
  }
  SaveTag(){
    //@ts-ignore
    if(this.tagName === '' || this.tagType.value === undefined || !this.url){
      this.apiService.Error("You have to fill all inputs");
      return;
    }
    //@ts-ignore
    this.mainService.createTag(this.locales, this.tagType.value, this.url);
    this.router.navigate(['/home']);
    this.mainService.changeMenu(2);
    this.mainService.changeUpdated(true);
  }
  deleteTag(id){
    this.mainService.deleteTag(id);
  }
  editTag(id){
    this.editingId = id;
    this.mainService.changeMenu(20);
    this.mainService.editTag(id);
  }
  Edit(){
    if(this.inpTag.nativeElement.value === '' || this.inpTag.nativeElement.value === undefined){
      this.apiService.Error("You have to fill all inputs");
      return;
    }
    // this.mainService.Edit(this.editingId, this.tagType.value, this.locales);
    //edit hazirlanmayib
  }
  files: any = [];

  uploadFile(event) {
    const files = event;
    if(files.length === 0){
      return;
    }
    if(files.length > 1){
      this.apiService.Error("You can select only one image");
    }
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.apiService.Error("Only images are supported.");
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      //@ts-ignore
      document.getElementsByClassName('uploadfilecontainer')[0].style.backgroundImage = "url('" + reader.result + "')";
      this.url = reader.result;
    }
  }
  deleteAttachment(index) {
    this.files.splice(index, 1);
  }
  changeInp(event){
    this.mainService.changeUpdated(false);
    this.tagName = event.target.value;
    let lang;
    if(this.language[0]){
      lang = 'az';
    }
    if(this.language[1]){
      lang = 'en';
    }
    if(this.language[2]){
      lang = 'ru';
    }
    if(this.language[3]){
      lang = 'tr';
    }
    if(this.locales.length === 0){
      this.locales.push({title: this.tagName, locale: lang});
    }
    else{
      let newTag = this.locales.find(locale => {
        return locale.locale === lang;
      });
      if(newTag === undefined){
        this.locales.push({title: this.tagName, locale: lang});
      }
      else{
        let i = 0;
        for(let locale of this.locales){
          if(locale.locale === lang){
            this.locales[i].title = this.tagName;
          }
          i++;
        }
      }
    }
    console.log(this.locales);
  }
  tagStatusChange(status, id){
    this.mainService.changeTagStatus(status === 1 ? 0 : 1, id);
  }
}

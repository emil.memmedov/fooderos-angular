import { Injectable } from "@angular/core";
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})
export class logOutService{
  constructor(private router: Router){}

  logOut(){
    localStorage.removeItem('fooderos');
    this.router.navigate(['/login']);
  }
}

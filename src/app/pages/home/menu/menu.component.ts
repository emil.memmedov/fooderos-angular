import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../main/main.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit{
  icons = [
    ['dashboard', 'Dashboard'],
    ['list_alt', 'Sifarişlər'],
    ['table_chart', 'Təqlər'],
    ['assignment', 'Kataloq'],
    ['fastfood', 'Məhsullar'],
    ['storefront', 'Partnyorlar'],
    ['supervisor_account', 'Müştərilər'],
    ['supervised_user_circle', 'İstifadəçilər'],
    ['grade', 'Rəylər'],
    ['loyalty', 'Companiyalar'],
    ['map', 'Xəritə'],
    ['settings', 'Tənzimləmələr']
  ];
  active = [];

  constructor(private router: Router, private mainService: MainService) {
    for(let i = 0; i< this.icons.length; i++){
      this.active.push(false);
    }
  }

  ngOnInit(): void{

  }

  menuClick(i){
    for(let inn = 0; inn < this.icons.length; inn++){
      this.active[inn] = false;
    }
    this.active[i] = true;
    this.router.navigate(['/home']);
    this.mainService.changeMenu(i);
  }

}

import { EventEmitter, Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class errorService {
  // error = new Subject();
  error = new EventEmitter<{error: boolean, message: string}>();
}

import { Component, OnInit } from '@angular/core';
import { errorService } from './error.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
  popup = false;
  errorMessage = "error var error";

  constructor(private errorService: errorService) { }

  ngOnInit(): void {
    this.errorService.error.subscribe(errorObj =>{
      //@ts-ignore
      this.popup = errorObj.error;
      //@ts-ignore
      this.errorMessage = errorObj.message;
      //@ts-ignore
      if(errorObj.error){
        setTimeout(() => {
          this.popup = false;
        }, 3000);
      }
    });
  }
}

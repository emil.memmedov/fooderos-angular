import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../ApiService.service';
import { errorService } from '../popup/error.service';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {
  @ViewChild('timer') timer;
  verificationFormGroup: FormGroup;
  name: string;
  number: string;
  code;
  error = false;
  expire = false;
  constructor(private apiService: ApiService, private errorService: errorService) { }

  ngOnInit(): void {
    this.errorService.error.subscribe(errorObj => {
      //@ts-ignore
      this.error = errorObj.error;
    });
    console.log(this.apiService.name);
    this.name = this.apiService.name;
    this.number = this.apiService.number;
    this.code = this.apiService.countryCode.phone_code;
    this.verificationFormGroup = new FormGroup({
      otp: new FormControl(null)
    });
    this.timer.begin();
  }

  onSubmit(){
    this.apiService.adminLogin(this.verificationFormGroup.value.otp);
  }
  handleEvent(event){
    if(event.action === 'done'){
      this.error = true;
      this.expire = true;
    }
  }
  Resend(){
    this.apiService.Resend();
    this.timer.restart();
    this.error = false;
    this.expire = false;
  }
}

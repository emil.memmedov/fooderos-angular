import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../ApiService.service';

@Injectable({providedIn: 'root'})
export class VerificationGuard implements CanActivate{
  constructor(private router: Router, private apiService: ApiService){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable <boolean> | Promise<boolean> | boolean{
    if(this.apiService.login){
      return true;
    }
    else{
      this.router.navigate(['/login']);
    }
  }
}

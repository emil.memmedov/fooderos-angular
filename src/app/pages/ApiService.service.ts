import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from './user';
import { Subject } from 'rxjs';
import { errorService } from './popup/error.service';

@Injectable({providedIn: 'root'})
export class ApiService{
  rest = {
    url: 'https://dev-api.fooderos.com/api/',
    admin_key: 'PXvr6k9RyeauDkLqbbcREK2PlpBFeWij'
  };
  countryCode;
  name: string;
  number: string;
  phone_code: number;
  user: User;
  login = false;
  constructor(private http: HttpClient, private router: Router, private errorService: errorService){}

  Error(message){
    this.errorService.error.emit({error: true, message: message});
  }
  GetCountryCodes(){
    return this.http.get(this.rest.url + 'v1/countries', {
      headers: new HttpHeaders({
        'ApplicationKey': this.rest.admin_key
      })
    });
  }
  GetOtp(data, countryCodes){
    this.number = data.phone;
    this.http.post(this.rest.url + 'v1/send/otp', data, {
      headers: new HttpHeaders({
        'ApplicationKey': this.rest.admin_key,
        'Content-Type': 'application/json'
      })
    }).subscribe(response =>{
      //@ts-ignore
      this.name = response.content.name;
      this.countryCode = countryCodes.find(cntry =>{
        return + cntry.id === +data.country_id;
      });
      this.login = true;
      this.router.navigate(['/verification']);
    },
    error =>{
      if(error.error.error.phone){
        this.Error(error.error.error.phone);
      }
      else{
        this.Error( "Number Not Found");
      }
    });
  }
  adminLogin(otp: string){
    this.http.post(this.rest.url + 'v1/login', {country_id: this.countryCode.id, phone: this.number, otp: +otp}, {
      headers: new HttpHeaders({
        'ApplicationKey': this.rest.admin_key,
        'Content-Type': 'application/json'
      })
    }).subscribe(response =>{
      //775787788
      //@ts-ignore
      this.user = response.content;
      localStorage.setItem('fooderos', JSON.stringify(this.user));
      this.router.navigate(['/home']);
    },
    error => {
      console.log("EEROOOR", error);
      if(error.error.error.phone){
        this.Error(error.error.error.phone);
      }
      else{
        this.Error("Security code is not correct");
      }
    });
  }
  Resend(){
    this.http.post(this.rest.url + 'v1/send/otp', {phone: this.number, country_id: this.countryCode.id}, {
      headers: new HttpHeaders({
        'ApplicationKey': this.rest.admin_key,
        'Content-Type': 'application/json'
      })
    }).subscribe(response =>{
      //@ts-ignore
      this.name = response.content.name;
      this.router.navigate(['/verification']);
    },
    error =>{
      if(error.error.error.phone){
        this.Error(error.error.error.phone);
      }
      else{
        this.Error("Number Not Found");
      }
    });
  }
}

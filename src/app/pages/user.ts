export class User{
  constructor(
    public is_phone_verified: number,
    public module_permissions: object[],
    public _expires_in: number,
    public _token: string
  ){}
  get token(){
    return this._token;
  }
  get expireTime(){
    return this._expires_in;
  }
}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
@Injectable({providedIn: 'root'})
export class LoginGuard implements CanActivate{
  constructor(private router: Router){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable <boolean> | Promise<boolean> | boolean{
    if(!localStorage.getItem('fooderos')){
      return true;
    }
    else{
      this.router.navigate(['/home']);
    }
  }
}

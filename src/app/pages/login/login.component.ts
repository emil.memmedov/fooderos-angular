import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../ApiService.service';
import { errorService } from '../popup/error.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginFormGroup: FormGroup;
  countryCodes;
  error = false;
  constructor(private apiService: ApiService, private errorService: errorService) { }

  ngOnInit(): void {
    this.loginFormGroup = new FormGroup({
      countryCodes: new FormControl('1'),
      phoneNumber: new FormControl(null)
    });
    this.apiService.GetCountryCodes().subscribe(data =>{
      //@ts-ignore
      this.countryCodes = data.content;
    });
    this.errorService.error.subscribe(errorObj =>{
      //@ts-ignore
      this.error = errorObj.error;
    });
  }
  onSubmit(){
    this.apiService.GetOtp({
      phone: this.loginFormGroup.value.phoneNumber,
      country_id: this.loginFormGroup.value.countryCodes
    }, this.countryCodes);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { LoginGuard } from './pages/login/loginGuard.service';
import { VerificationComponent } from './pages/verification/verification.component';
import {VerificationGuard} from './pages/verification/verificationGuard.service';
import {HomeGuard} from './pages/home/homeGuard.service';
import { createGuardPage } from './pages/home/main/createPageGuard.service';

const routes: Routes = [
  {path: 'login', canActivate: [LoginGuard], component: LoginComponent},
  {path: 'verification', canActivate: [VerificationGuard], component: VerificationComponent},
  {path: 'home', canActivate: [HomeGuard] , component: HomeComponent},
  {path: 'home/create', component: HomeComponent, canDeactivate: [createGuardPage]},
  {path: 'home/edit', component: HomeComponent, canDeactivate: [createGuardPage]},
  {path: '', redirectTo: 'login', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
